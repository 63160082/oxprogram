/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ht.ox;

import java.util.Scanner;

/**
 *
 * @author ACER
 */
public class OX {
    private char[][] table;         
    private char currentPlayer;           
    private int rows = 3;
    private int columns = 3;
    
    public static Scanner kb = new Scanner(System.in); // the input Scanner
    
    public OX() {
        table = new char[rows][columns];           
        currentPlayer = 'X';            
        initializeBoard();
    }
    
    public void initializeBoard() {
        for(int i = 0; i < rows; i++) {
            for(int j = 0; j < columns; j++) {
                table[i][j] = '-';
            }
        }    
}   

        public void printBoard() {
        for(int i = 0; i < rows; i++) {
            for(int j = 0; j < columns; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println("");

        }
    }
    
    public boolean isBoardFull() {
        for(int i = 0; i < rows; i++) {
            for(int j = 0; j < columns; j++) {
                if (table[i][j] == '-') {
                    return false;
                }
            }
        }
        return true;
    }
    
    public boolean checkForWinner() {
        return (checkRows() || checkColumns() || checkDiagonals());
    }    
 
    private boolean checkRows() {
        for (int i = 0; i < rows; i++) {
            if (table[i][0] != '-' && table[i][0] == table[i][1] && table[i][0] == table[i][2]) {
                return true;
            }
        }
        return false;
    }    

    private boolean checkColumns() {
        for (int i = 0; i < columns; i++) {
            if (table[0][i] != '-' && table[0][i] == table[1][i] && table[0][i] == table[2][i]) {
                return true;
            }
        }
        return false;
    }
    
    private boolean checkDiagonals() {
        return (table[0][0] != '-' && table[0][0] == table[1][1] 
                && table[0][0] == table[2][2]) 
                ||
               (table[0][2] != '-' && table[0][2] == table[1][1] 
                && table[0][2] == table[2][0]);
                
    }

    public void changePlayer() {
        currentPlayer = (currentPlayer == 'X') ? 'O' : 'X';
    }
    
    public char printPlayer() {
        return currentPlayer;
    }

    public void updateBoard() {
        boolean validInput = false;     
        do {
         if (currentPlayer == 'X') {
             System.out.println("Turn X");
            System.out.println("Please input row , col : ");
         } else {
             System.out.println("Turn O");
            System.out.println("Please input row , col : ");
         }
         int row = kb.nextInt() - 1; 
         int col = kb.nextInt() - 1;
         
         if (row >= 0 && row < rows && col >= 0 && col < columns 
             && table[row][col] == '-') {
            table[row][col] = currentPlayer; 
            validInput = true;  
         }
         
      } while (!validInput); 
    }    

    public static void main(String[] args) {       
        OX ox = new OX();
        boolean gameStatus = false;            
        
        System.out.println("Welcome to OX Game");
        ox.printBoard();
        
        do {
            ox.updateBoard();
            ox.printBoard();

            if (ox.checkForWinner()) {
                System.out.println(">>>" + ox.printPlayer() + " Win <<<" );
                gameStatus = true;
            }
            if (ox.isBoardFull()) {
                System.out.println(">>> Draw <<<");
                gameStatus = true;
            }
            
            ox.changePlayer();  
        } while (gameStatus == false);         
        
    }
}
